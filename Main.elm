module Main exposing (..)

{-| Demonstration project for Db.elm
Based on Evan's todo-mvc-example
-}

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Json
import String
import Todo
import Archive
import Db
import DbConfiguration


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions =
            \_ ->
                Sub.batch
                    [ Sub.map TODO <| Db.answerPort Todo.HandleDbAnswer
                    , Sub.map ARCHIVE <| Archive.subscriptions
                    ]
        }


type alias Model =
    { todo : Todo.Todo
    , archive : Archive.Archive
    }


type Msg
    = TODO Todo.Msg
    | ARCHIVE Archive.Msg


init : ( Model, Cmd Msg )
init =
    let
        ( todo, todoCmd ) =
            Todo.init

        ( archive, archiveCmd ) =
            Archive.init
    in
        { todo = todo
        , archive = archive
        }
            ! [ Cmd.map TODO todoCmd
              , Cmd.map ARCHIVE archiveCmd
              ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TODO msg ->
            let
                ( todo, cmd ) =
                    Todo.update msg model.todo
            in
                { model | todo = todo }
                    ! [ Cmd.map TODO cmd ]

        ARCHIVE msg ->
            let
                ( archive, cmd ) =
                    Archive.update msg model.archive
            in
                { model | archive = archive }
                    ! [ Cmd.map ARCHIVE cmd ]



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ class "todomvc-wrapper"
        , style [ ( "visibility", "hidden" ) ]
        ]
        [ Html.map TODO <| Todo.view model.todo
        , Html.map ARCHIVE <| Archive.view model.archive
        , infoFooter
        ]


infoFooter : Html msg
infoFooter =
    footer [ class "info" ]
        [ p [] [ text "Double-click to edit a todo!" ]
        , p []
            [ text "Based on "
            , a [ href "https://github.com/evancz" ] [ text "Evan Czaplicki's" ]
            , text " "
            , a [ href "https://github.com/evancz/elm-todomvc" ] [ text "TodoMVC" ]
            ]
        ]
