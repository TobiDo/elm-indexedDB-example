port module Db exposing (..)

import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline as Pipeline


{-
   This module takes care of encapsulating requests to IndexedDB
-}


type alias ObjectStore =
    String


type alias DbAnswer msg =
    { address : String
    , msg : msg
    , result : Decode.Value
    }


type BroadcastType
    = UPDATED
    | DELETED


type alias DbBroadcast =
    { method : BroadcastType
    , storeName : ObjectStore
    , object : Decode.Value
    }


type DbRequest
    = ADD ObjectStore Encode.Value
    | GET_ALL ObjectStore
    | GET ObjectStore Encode.Value
    | GET_LIST ObjectStore (List Encode.Value)
    | GET_INDEX_VALUES DbIndex
    | GET_BY_INDEX DbIndex Encode.Value
    | PUT ObjectStore Encode.Value
    | DELETE ObjectStore Encode.Value


type alias DbPackage msg =
    { returnToMsg : msg
    , request : DbRequest
    }


type alias Address =
    String


type alias DbIndex =
    { storeName : ObjectStore
    , indexName : String
    }


dbUpdate :
    Decode.Decoder dbMsg
    -> (dbMsg -> Decode.Value -> model -> model)
    -> Address
    -> Decode.Value
    -> model
    -> ( model, Cmd msg )
dbUpdate msgDecoder update address value model =
    case Decode.decodeValue (dbAnswerDecoder msgDecoder) value of
        Ok dbAnswer ->
            if dbAnswer.address /= address then
                model ! []
            else
                update dbAnswer.msg dbAnswer.result model ! []

        Err e ->
            model ! []


encodeDbIndex : DbIndex -> Encode.Value
encodeDbIndex dbIndex =
    Encode.object
        [ ( "storeName", Encode.string dbIndex.storeName )
        , ( "indexName", Encode.string dbIndex.indexName )
        ]


broadcastTypeDecoder : Decode.Decoder BroadcastType
broadcastTypeDecoder =
    Decode.string
        |> Decode.andThen
            (\s ->
                if s == "UPDATED" then
                    Decode.succeed UPDATED
                else if s == "DELETED" then
                    Decode.succeed DELETED
                else
                    Decode.fail <| "No broadcast type of name " ++ s
            )


dbBroadcastDecoder : Decode.Decoder DbBroadcast
dbBroadcastDecoder =
    Pipeline.decode DbBroadcast
        |> Pipeline.required "method" broadcastTypeDecoder
        |> Pipeline.required "storeName" Decode.string
        |> Pipeline.required "object" Decode.value


dbAnswerDecoder : Decode.Decoder msg -> Decode.Decoder (DbAnswer msg)
dbAnswerDecoder msgDecoder =
    Pipeline.decode DbAnswer
        |> Pipeline.required "address" Decode.string
        |> Pipeline.required "msg" msgDecoder
        |> Pipeline.required "result" Decode.value


encodeDbRequest : DbRequest -> Encode.Value
encodeDbRequest request =
    case request of
        ADD storeName object ->
            Encode.object
                [ ( "action", Encode.string "ADD" )
                , ( "storeName", Encode.string storeName )
                , ( "object", object )
                ]

        GET_ALL storeName ->
            Encode.object
                [ ( "action", Encode.string "GET_ALL" )
                , ( "storeName", Encode.string storeName )
                ]

        GET storeName key ->
            Encode.object
                [ ( "action", Encode.string "GET" )
                , ( "storeName", Encode.string storeName )
                , ( "keyValue", key )
                ]

        GET_LIST storeName keyList ->
            Encode.object
                [ ( "action", Encode.string "GET_LIST" )
                , ( "storeName", Encode.string storeName )
                , ( "keyValueList", Encode.list keyList )
                ]

        GET_BY_INDEX index indexValue ->
            Encode.object
                [ ( "action", Encode.string "GET_BY_INDEX" )
                , ( "storeName", Encode.string index.storeName )
                , ( "indexName", Encode.string index.indexName )
                , ( "indexValue", indexValue )
                ]

        GET_INDEX_VALUES index ->
            Encode.object
                [ ( "action", Encode.string "GET_INDEX_VALUES" )
                , ( "storeName", Encode.string index.storeName )
                , ( "indexName", Encode.string index.indexName )
                ]

        PUT storeName object ->
            Encode.object
                [ ( "action", Encode.string "PUT" )
                , ( "storeName", Encode.string storeName )
                , ( "object", object )
                ]

        DELETE storeName keyValue ->
            Encode.object
                [ ( "action", Encode.string "DELETE" )
                , ( "storeName", Encode.string storeName )
                , ( "keyValue", keyValue )
                ]


encodePackageWithAddress : Address -> (msg -> Encode.Value) -> DbPackage msg -> Encode.Value
encodePackageWithAddress address msgEncoder dbPackage =
    Encode.object
        [ ( "address", Encode.string address )
        , ( "msg", msgEncoder dbPackage.returnToMsg )
        , ( "request", encodeDbRequest dbPackage.request )
        ]


sendRequest : String -> (dbMsg -> Encode.Value) -> DbPackage dbMsg -> Cmd msg
sendRequest address dbMsgEncoder =
    sendPort << encodePackageWithAddress address dbMsgEncoder



-- Ports


port receiveBroadcasts : (Encode.Value -> msg) -> Sub msg


port sendPort : Encode.Value -> Cmd msg


port answerPort : (Encode.Value -> msg) -> Sub msg
