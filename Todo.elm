port module Todo exposing (..)

{-| TodoMVC implemented in Elm, using plain HTML and CSS for rendering.

This application is broken up into three key parts:

1.  Model - a full definition of the application's state
2.  Update - a way to step the application state forward
3.  View - a way to visualize our application state with HTML

This clean division of concerns is a core part of Elm. You can read more about
this in <http://guide.elm-lang.org/architecture/index.html>

-}

import Dom
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Keyed as Keyed
import Html.Lazy exposing (lazy, lazy2)
import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline as Pipeline
import String
import Task
import Db
import DbConfiguration


-- model for list of active entries


type alias Todo =
    { entries : List Entry
    , field : String
    , uid : Int
    , visibility : String
    }


type alias Entry =
    { description : String
    , status : Status
    , editing : Bool
    , id : Int
    }


type Status
    = ACTIVE Bool
    | ARCHIVED


isCompleted : Entry -> Bool
isCompleted entry =
    case entry.status of
        ACTIVE isCompleted ->
            isCompleted

        ARCHIVED ->
            False


isToBeDone : Entry -> Bool
isToBeDone entry =
    case entry.status of
        ACTIVE isCompleted ->
            not isCompleted

        ARCHIVED ->
            False


emptyModel : Todo
emptyModel =
    { entries = []
    , visibility = "All"
    , field = ""
    , uid = 0
    }


newEntry : String -> Int -> Entry
newEntry desc id =
    { description = desc
    , status = ACTIVE False
    , editing = False
    , id = id
    }


init : ( Todo, Cmd Msg )
init =
    Maybe.withDefault emptyModel Nothing
        ! [ Db.GET_ALL DbConfiguration.todoStore
                |> Db.DbPackage LoadList
                |> addSubModelInfo
          ]


{-| Creates a (Db)-command from a DbPackage adding the return address
-}
addSubModelInfo : Db.DbPackage DbMsg -> Cmd Msg
addSubModelInfo =
    Db.sendRequest DbConfiguration.todo (Encode.string << msgToString)



-- UPDATE


{-| Users of our app can trigger messages by clicking and typing. These
messages are fed into the `update` function as they occur, letting us react
to them.
-}
type Msg
    = NoOp
    | UpdateField String
    | EditingEntry Int Bool
    | ShowEntry Int
    | UpdateEntry Int String
    | SaveEntry Int
    | Add
    | Delete Int
    | DeleteComplete
    | Check Int Bool
    | CheckAll Bool
    | ChangeVisibility String
    | HandleDbAnswer Decode.Value
    | DbResponse DbMsg Decode.Value


type DbMsg
    = LoadList
    | Ignore


dbUpdate : DbMsg -> Decode.Value -> Todo -> Todo
dbUpdate msg value todo =
    case msg of
        LoadList ->
            case Decode.decodeValue (Decode.list entryDecoder) value of
                Ok allEntries ->
                    let
                        max =
                            allEntries
                                |> List.map .id
                                |> List.maximum
                                |> Maybe.withDefault 0

                        activeEntries =
                            List.filter ((/=) ARCHIVED << .status) allEntries
                    in
                        { todo | entries = activeEntries, uid = max + 1 }

                Err e ->
                    todo

        Ignore ->
            todo



-- How we update our Model on a given Msg?


update : Msg -> Todo -> ( Todo, Cmd Msg )
update msg todo =
    case msg of
        NoOp ->
            todo ! []

        Add ->
            let
                entry =
                    newEntry todo.field todo.uid
            in
                if String.isEmpty todo.field then
                    todo ! []
                else
                    { todo
                        | uid = todo.uid + 1
                        , field = ""
                        , entries = todo.entries ++ [ entry ]
                    }
                        ! [ encodeEntry entry
                                |> Db.ADD DbConfiguration.todoStore
                                |> Db.DbPackage LoadList
                                |> addSubModelInfo
                          ]

        DbResponse dbMsg value ->
            dbUpdate dbMsg value todo ! []

        UpdateField str ->
            { todo | field = str }
                ! []

        EditingEntry id isEditing ->
            let
                updateEntry t =
                    if t.id == id then
                        { t | editing = isEditing }
                    else
                        t

                focus =
                    Dom.focus ("todo-" ++ toString id)
            in
                { todo | entries = List.map updateEntry todo.entries }
                    ! [ Task.attempt (\_ -> NoOp) focus ]

        ShowEntry id ->
            todo ! []

        UpdateEntry id task ->
            case List.head <| List.filter (\e -> e.id == id) todo.entries of
                Just oldEntry ->
                    let
                        newEntry =
                            { oldEntry | description = task }
                    in
                        let
                            updateEntry t =
                                if t.id == id then
                                    newEntry
                                else
                                    t
                        in
                            { todo | entries = List.map updateEntry todo.entries }
                                ! []

                Nothing ->
                    todo ! []

        SaveEntry id ->
            case List.head <| List.filter (\e -> e.id == id) todo.entries of
                Just oldEntry ->
                    let
                        newEntry =
                            { oldEntry | editing = False }
                    in
                        let
                            updateEntry t =
                                if t.id == id then
                                    newEntry
                                else
                                    t
                        in
                            { todo | entries = List.map updateEntry todo.entries }
                                ! [ encodeEntry newEntry
                                        |> Db.PUT DbConfiguration.todoStore
                                        |> Db.DbPackage LoadList
                                        |> addSubModelInfo
                                  ]

                Nothing ->
                    todo ! []

        Delete id ->
            { todo | entries = List.filter (\t -> t.id /= id) todo.entries }
                ! [ Encode.int id
                        |> Db.DELETE DbConfiguration.todoStore
                        |> Db.DbPackage Ignore
                        |> addSubModelInfo
                  ]

        DeleteComplete ->
            let
                completed =
                    List.filter isCompleted todo.entries
            in
                { todo | entries = List.filter isToBeDone todo.entries }
                    ! List.map
                        (\e ->
                            encodeEntry { e | status = ARCHIVED }
                                |> Db.PUT DbConfiguration.todoStore
                                |> Db.DbPackage Ignore
                                |> addSubModelInfo
                        )
                        completed

        Check id isCompleted ->
            let
                updateEntry e ( l, maybeEntry ) =
                    if e.id == id then
                        let
                            updatedEntry =
                                { e | status = ACTIVE isCompleted }
                        in
                            ( updatedEntry :: l, Just updatedEntry )
                    else
                        ( e :: l, maybeEntry )
            in
                let
                    ( updatedEntries, updatedEntry ) =
                        List.foldr updateEntry ( [], Nothing ) todo.entries
                in
                    { todo | entries = updatedEntries }
                        ! (Maybe.withDefault [] <|
                            Maybe.map
                                (encodeEntry
                                    >> Db.PUT DbConfiguration.todoStore
                                    >> Db.DbPackage Ignore
                                    >> addSubModelInfo
                                    >> List.singleton
                                )
                                updatedEntry
                          )

        CheckAll isCompleted ->
            let
                updateEntry t =
                    case t.status of
                        ARCHIVED ->
                            t

                        ACTIVE _ ->
                            { t | status = ACTIVE isCompleted }
            in
                let
                    entriesNew =
                        List.map updateEntry todo.entries
                in
                    { todo | entries = entriesNew }
                        ! (List.map
                            (encodeEntry
                                >> Db.PUT DbConfiguration.todoStore
                                >> Db.DbPackage Ignore
                                >> addSubModelInfo
                            )
                           <|
                            List.filter (\e -> e.status /= ARCHIVED) entriesNew
                          )

        ChangeVisibility visibility ->
            { todo | visibility = visibility }
                ! []

        HandleDbAnswer value ->
            Db.dbUpdate
                msgDecoder
                dbUpdate
                DbConfiguration.todo
                value
                todo



-- VIEW


view : Todo -> Html Msg
view todo =
    section
        [ class "todoapp" ]
        [ lazy viewInput todo.field
        , lazy2 viewEntries todo.visibility todo.entries
        , lazy2 viewControls todo.visibility todo.entries
        ]


viewInput : String -> Html Msg
viewInput task =
    header
        [ class "header" ]
        [ h1 [] [ text "todos" ]
        , input
            [ class "new-todo"
            , placeholder "What needs to be done?"
            , autofocus True
            , value task
            , name "newTodo"
            , onInput UpdateField
            , onEnter Add
            ]
            []
        ]


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Decode.succeed msg
            else
                Decode.fail "not ENTER"
    in
        on "keydown" (Decode.andThen isEnter keyCode)



-- VIEW ALL ENTRIES


viewEntries : String -> List Entry -> Html Msg
viewEntries visibility entries =
    let
        isVisible todo =
            case visibility of
                "Completed" ->
                    isCompleted todo

                "Active" ->
                    isToBeDone todo

                _ ->
                    True

        allCompleted =
            List.all isCompleted <| List.filter ((/=) ARCHIVED << .status) entries

        cssVisibility =
            if List.isEmpty entries then
                "hidden"
            else
                "visible"
    in
        section
            [ class "main"
            , style [ ( "visibility", cssVisibility ) ]
            ]
            [ input
                [ class "toggle-all"
                , type_ "checkbox"
                , name "toggle"
                , checked allCompleted
                , onClick (CheckAll (not allCompleted))
                ]
                []
            , label
                [ for "toggle-all" ]
                [ text "Mark all as complete" ]
            , Keyed.ul [ class "todo-list" ] <|
                List.map viewKeyedEntry (List.filter isVisible entries)
            ]



-- VIEW INDIVIDUAL ENTRIES


viewKeyedEntry : Entry -> ( String, Html Msg )
viewKeyedEntry todo =
    ( toString todo.id, lazy viewEntry todo )


viewEntry : Entry -> Html Msg
viewEntry todo =
    li
        [ classList [ ( "completed", isCompleted todo ), ( "editing", todo.editing ) ] ]
        [ div
            [ class "view" ]
            [ input
                [ class "toggle"
                , type_ "checkbox"
                , checked <| isCompleted todo
                , onClick (Check todo.id (not <| isCompleted todo))
                ]
                []
            , label
                [ onDoubleClick (EditingEntry todo.id True)
                , onClick (ShowEntry todo.id)
                ]
                [ text todo.description ]
            , button
                [ class "destroy"
                , onClick (Delete todo.id)
                ]
                []
            ]
        , input
            [ class "edit"
            , value todo.description
            , name "title"
            , id ("todo-" ++ toString todo.id)
            , onInput (UpdateEntry todo.id)
            , onBlur (SaveEntry todo.id)
            , onEnter (SaveEntry todo.id)
            ]
            []
        ]



-- VIEW CONTROLS AND FOOTER


viewControls : String -> List Entry -> Html Msg
viewControls visibility entries =
    let
        entriesCompleted =
            List.length (List.filter isCompleted entries)

        entriesLeft =
            List.length entries - entriesCompleted
    in
        footer
            [ class "footer"
            , hidden (List.isEmpty entries)
            ]
            [ lazy viewControlsCount entriesLeft
            , lazy viewControlsFilters visibility
            , lazy viewControlsClear entriesCompleted
            ]


viewControlsCount : Int -> Html Msg
viewControlsCount entriesLeft =
    let
        item_ =
            if entriesLeft == 1 then
                " item"
            else
                " items"
    in
        span
            [ class "todo-count" ]
            [ strong [] [ text (toString entriesLeft) ]
            , text (item_ ++ " left")
            ]


viewControlsFilters : String -> Html Msg
viewControlsFilters visibility =
    ul
        [ class "filters" ]
        [ visibilitySwap "#/" "All" visibility
        , text " "
        , visibilitySwap "#/active" "Active" visibility
        , text " "
        , visibilitySwap "#/completed" "Completed" visibility
        ]


visibilitySwap : String -> String -> String -> Html Msg
visibilitySwap uri visibility actualVisibility =
    li
        [ onClick (ChangeVisibility visibility) ]
        [ a [ href uri, classList [ ( "selected", visibility == actualVisibility ) ] ]
            [ text visibility ]
        ]


viewControlsClear : Int -> Html Msg
viewControlsClear entriesCompleted =
    button
        [ class "clear-completed"
        , hidden (entriesCompleted == 0)
        , onClick DeleteComplete
        ]
        [ text ("Archive completed (" ++ toString entriesCompleted ++ ")")
        ]


encodeEntry : Entry -> Encode.Value
encodeEntry entry =
    Encode.object
        [ ( "desc", Encode.string entry.description )
        , ( "id", Encode.int entry.id )
        , ( "status", encodeStatus entry.status )
        ]


encodeStatus : Status -> Encode.Value
encodeStatus status =
    case status of
        ACTIVE False ->
            Encode.string "TO_BE_DONE"

        ACTIVE True ->
            Encode.string "COMPLETED"

        ARCHIVED ->
            Encode.string "ARCHIVED"


msgToString : DbMsg -> String
msgToString msg =
    case msg of
        LoadList ->
            "LOAD_LIST"

        Ignore ->
            "IGNORE"


msgDecoder : Decode.Decoder DbMsg
msgDecoder =
    Decode.string
        |> Decode.andThen
            (\s ->
                if s == "LOAD_LIST" then
                    Decode.succeed <| LoadList
                else
                    Decode.fail <| "No msg of name " ++ s
            )


statusDecoder : Decode.Decoder Status
statusDecoder =
    Decode.string
        |> Decode.andThen
            (\s ->
                if s == "TO_BE_DONE" then
                    Decode.succeed <| ACTIVE False
                else if s == "COMPLETED" then
                    Decode.succeed <| ACTIVE True
                else if s == "ARCHIVED" then
                    Decode.succeed ARCHIVED
                else
                    Decode.fail <| "Status " ++ s ++ " does not exist."
            )


entryDecoder : Decode.Decoder Entry
entryDecoder =
    Pipeline.decode Entry
        |> Pipeline.required "desc" Decode.string
        |> Pipeline.required "status" statusDecoder
        |> Pipeline.hardcoded False
        |> Pipeline.required "id" Decode.int
