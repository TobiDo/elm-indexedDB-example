module DbConfiguration exposing (..)

import Db exposing (..)


{-

   This module contains configuration of sub-module addresses and the database schema
   So far this is just a description of the existing schema.
   It would, of course, be desirable to have way to manage DB migration via elm.

-}


todoStore : ObjectStore
todoStore =
    "todo"


statusIndex : DbIndex
statusIndex =
    { storeName = todoStore
    , indexName = "status"
    }


todo : Address
todo =
    "todo"


archive : Address
archive =
    "archive"
