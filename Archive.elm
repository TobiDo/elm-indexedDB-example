module Archive exposing (..)

import Todo
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Decode
import Json.Encode as Encode
import Db
import DbConfiguration


type alias Archive =
    { tasks : List Todo.Entry
    }


type Msg
    = ClearArchive
    | HandleDbBroadcast Decode.Value
    | HandleDbAnswer Decode.Value
    | DbResponse DbMsg Decode.Value


type DbMsg
    = SetList
    | Ignore


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ Db.receiveBroadcasts HandleDbBroadcast
        , Db.answerPort HandleDbAnswer
        ]


init : ( Archive, Cmd Msg )
init =
    Archive []
        ! [ sendPackage <|
                Db.DbPackage SetList <|
                    Db.GET_BY_INDEX DbConfiguration.statusIndex <|
                        Encode.string "ARCHIVED"
          ]


dbUpdate : DbMsg -> Decode.Value -> Archive -> Archive
dbUpdate msg value model =
    case msg of
        SetList ->
            case Decode.decodeValue (Decode.list Todo.entryDecoder) value of
                Ok taskList ->
                    { model | tasks = taskList }

                Err _ ->
                    model

        Ignore ->
            model


update : Msg -> Archive -> ( Archive, Cmd Msg )
update msg model =
    case msg of
        ClearArchive ->
            { model | tasks = [] }
                ! List.map
                    (.id
                        >> Encode.int
                        >> Db.DELETE DbConfiguration.todoStore
                        >> Db.DbPackage Ignore
                        >> sendPackage
                    )
                    model.tasks

        DbResponse msg value ->
            dbUpdate msg value model ! []

        HandleDbBroadcast value ->
            case Decode.decodeValue Db.dbBroadcastDecoder value of
                Ok dbBroadcast ->
                    case dbBroadcast.method of
                        Db.DELETED ->
                            -- the only way to delete an archived entry is to delete
                            -- all of them via ClearArchive.
                            -- these kind of broadcasts can hence be ignored here
                            model ! []

                        Db.UPDATED ->
                            if dbBroadcast.storeName == DbConfiguration.todoStore then
                                (Decode.decodeValue Todo.entryDecoder dbBroadcast.object
                                    |> Result.map (handleUpdate model)
                                    |> Result.withDefault model
                                )
                                    ! []
                            else
                                model ! []

                Err e ->
                    model ! []

        HandleDbAnswer value ->
            Db.dbUpdate
                msgDecoder
                dbUpdate
                DbConfiguration.archive
                value
                model


{-| Handle database broadcast for an entry.
If the entry is an archived one: add it to the archive list
if it is not already present. Otherwise update the corresponding entry.
-}
handleUpdate : Archive -> Todo.Entry -> Archive
handleUpdate model entry =
    case entry.status of
        Todo.ARCHIVED ->
            let
                updateOrInsert entry list =
                    if List.member entry.id <| List.map .id list then
                        List.map
                            (\e ->
                                if entry.id == e.id then
                                    entry
                                else
                                    e
                            )
                            list
                    else
                        list ++ [ entry ]
            in
                { model | tasks = updateOrInsert entry model.tasks }

        _ ->
            model


view : Archive -> Html Msg
view model =
    section
        [ class "todoapp" ]
        [ header
            [ class "header" ]
            [ h1
                []
                [ text "Archive" ]
            ]
        , ul
            []
          <|
            List.map (\t -> li [] [ text t.description ]) model.tasks
        , footer
            [ class "footer" ]
            [ span []
                [ button
                    [ onClick ClearArchive
                    , class "clear-completed"
                    ]
                    [ text "Clear archive" ]
                ]
            ]
        ]



-- Needed for DB communication via Db.elm


sendPackage : Db.DbPackage DbMsg -> Cmd Msg
sendPackage =
    Db.sendRequest DbConfiguration.archive (Encode.string << msgToString)


msgToString : DbMsg -> String
msgToString msg =
    case msg of
        SetList ->
            "SET_LIST"

        Ignore ->
            "IGNORE"


msgDecoder : Decode.Decoder DbMsg
msgDecoder =
    Decode.string
        |> Decode.andThen
            (\s ->
                if s == "SET_LIST" then
                    Decode.succeed <| SetList
                else
                    Decode.fail <| "No msg of name " ++ s
            )
