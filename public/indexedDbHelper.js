// rudimentary "Indexed DB" - interface

function openDB() {
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    var open = indexedDB.open('todo', 1);

    // Create object store and index
    open.onupgradeneeded = function() {
        var db = open.result;
        try {
            var todoStore = db.createObjectStore('todo', {keyPath: 'id'});
            todoStore.createIndex('status', 'status');
        }catch(e) {
            console.log(e);
        }
    };

    return open;
}


function performRequest (request) {
    var open = openDB();
    return new Promise (function (resolve, reject) {
        open.onsuccess = function() {
            var mode;
            if (request.action == 'PUT' || request.action == 'DELETE' || request.action == 'ADD') {
                mode = 'readwrite';
            }
            else {
                mode = 'readonly';
            }
            var storeName = request.storeName;
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction(storeName, mode);
            var store = tx.objectStore(storeName);
            var response = {};

            // Query the data
            var query;
            if (request.action == 'GET_ALL') {
                // get all objects of the store
                query = store.getAll();
                query.onsuccess = function () {
                    response.answer = query.result;
                    resolve(response);
                }
            }
            else if (request.action == 'GET') {
                // get object whose key matches key value (if present)
                var keyValue = request.keyValue;
                query = store.get(keyValue);
                query.onsuccess = function () {
                    response.answer = query.result;
                    resolve(response);
                }
            }
            else if (request.action == 'GET_LIST') {
                var keyValueList = request.keyValueList;
                if (keyValueList.length == 0) {
                    response.answer = [];
                    resolve(response);
                }
                var result = [];
                var query  = store.getAll();
                query.onsuccess = function () {
                   if (query.result != null) {
                   // TODO: do this right
                   response.answer = query.result.filter(u => keyValueList.some(kv => kv == u.id));
                   resolve(response);
                   };
                }
            }
            else if (request.action == 'GET_BY_INDEX') {
                // get all entries of store whose value at indexName matches indexValue
                var indexName = request.indexName;
                var index = store.index(indexName);
                query = index.getAll(request.indexValue);
                query.onsuccess = function () {
                    response.answer = query.result;
                    resolve(response);
                }
            }
            else if (request.action == 'GET_INDEX_VALUES') {
                // get list of distinct values that appear in the specified index
                var indexName = request.indexName;
                var index = store.index(indexName);
                var result = [];
                index.openCursor(null, 'nextunique').onsuccess = function(event) {
                    var cursor = event.target.result;
                    if(cursor) {
                        result.push(cursor.value.class);
                        cursor.continue();
                    }
                    else {
                        response.answer = result;
                        resolve(response);
                    }
                };
            }
            else if (request.action == 'ADD') {
                // add object - fails if an object with same key value already exists
                var dbRequest = store.add(request.object);
                dbRequest.onsuccess = function (result) {
                    response.answer = 'SUCCESS';
                    response.broadcast = broadcastSuccess('ADDED', request.storeName, request.object);
                    resolve(response);
                };
                dbRequest.onerror =  function () {
                    response.answer = 'ERROR';
                    resolve(response);
                };

            }
            else if (request.action == 'PUT') {
                // add or (if object with same key value exists) update
                var dbRequest = store.put(request.object);
                dbRequest.onsuccess = function (result) {
                    response.answer = 'SUCCESS';
                    response.broadcast = broadcastSuccess('UPDATED', request.storeName, request.object);
                    resolve(response);
                };
                dbRequest.onerror =  function () {
                    response.answer = 'ERROR';
                    resolve(response);
                };

            }
            else if (request.action == 'DELETE') {
                // delete object with corresponding key value
                var dbRequest = store.delete(request.keyValue);
                dbRequest.onsuccess = function (result) {
                    response.answer = 'SUCCESS';
                    response.broadcast = broadcastSuccess('DELETED', request.storeName, request.keyValue);
                    resolve(response);
                };
                dbRequest.onerror =  function () {
                    response.answer = 'ERROR';
                    resolve(response);
                };

            }
            else {
                // error handling
            }

            // Close the db when the transaction is done
            tx.oncomplete = function() {
                db.close();
            };
        }
    });
}

function broadcastSuccess(method, storeName, object) {
    return { method : method
           , storeName : storeName
           , object : object}
}